import http from '../http'

class UserDataService {
    getAll(page:number): Promise<any> {
        return http.get(`/users?page=${page}`);
    }
}

export default new UserDataService();