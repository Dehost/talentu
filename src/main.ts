import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Router from './router'

import Notifications from '@kyvg/vue3-notification'

createApp(App)
    .use(Router)
    .use(Notifications)
    .mount('#app')
