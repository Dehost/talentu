# Administración de usuarios

## Instalación dependencias
```
npm install
```

## Pre-requisitos
El proyecto talentu debe instalando dependencias y debe poner el archivo raíz .env.development:

Ejemplo:
VITE_API_URL='https://reqres.in/api'

### Compiles and hot-reloads for development
```
npm run dev
```

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

### Customize configuration
See [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup)